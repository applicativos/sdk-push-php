# SDK para gerenciado de PUSH - Sistema do cliente.
```php
<?php

require './vendor/autoload.php';
include './src/Push/config.php';
$push = new Sdk\Push\Push($config);

function debug($val) {
    echo '<pre>';
    print_r($val);
    echo '</pre>';
}


echo '<h1>Info</h1>';
debug($push->info());

echo '<h1>Devices Add</h1>';
debug($push->app(
        [
            'id' => '', // usar somente se for fazer update nos dados cadastrais do app.
            'nome' => 'Nome do Aplicativo',
            'status' => 1 // 0 - Inativo | 1 - Ativo | 9 - Excluido
        ]
    )
);

echo '<h1>Devices Add</h1>';
debug($push->devicesAdd(
        [
            'plataforma' => 'Android',
            'marca' => 'Samsung',
            'modelo' => 'aa' . rand(1000, 9999),
            'cordova' => rand(1, 99),
            'uuid' => md5(uniqid() . time()),
            'version' => rand(1000, 9999),
            'version_app' => rand(1000, 9999),
            'serial' => date('s') . md5(uniqid() . time()),
            'serial_unique' => date('s') . md5(uniqid() . time()),
            'status' => 1,
            'id_user' => 1,
            'tipo' => 'default'
        ]
    )
);

echo '<h1>All Devices</h1>';
debug($push->devices());

echo '<h1>Devices Params</h1>';
debug($push->devices(['marca' => 'Samsung']));

echo '<h1>Devices Edit</h1>';
debug($push->devicesEdit(1,
        [
            'plataforma' => 'Android',
            'marca' => 'Samsung',
            'modelo' => 'aa' . rand(1000, 9999),
            'cordova' => rand(1, 99),
            'uuid' => md5(uniqid() . time()),
            'version' => rand(1000, 9999),
            'version_app' => rand(1000, 9999),
            'serial' => date('s') . md5(uniqid() . time()),
            'serial_unique' => date('s') . md5(uniqid() . time()),
            'status' => 1,
            'id_user' => 1,
            'tipo' => 'default'
        ]
    )
);


echo '<h1>Devices Detalhes</h1>';
debug($push->devicesView(1));
echo '<h1>Mensagens Add</h1>';
debug($push->mensagensAdd(
              [
                  'msg' => 'Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.',
                  'extra' => 
                        'notification' => [
                            'title' => '',
                            'body' => '',
                            'icon' => '',
                            'sound' => '',
                            'badge' => '',
                            'tag' => '',
                            'color' => ''
                        ],
                        'data' => []
                  ],
                  'tipo' => 'default',
                  'status' => 1,
                  'data_hora_incio' => date('Y-m-d H:i:s', mktime(date('H') + 5))
              ]
      )
);

echo '<h1>Mensagens</h1>';
debug($push->mensagens());

echo '<h1>Mensagens Detalhes</h1>';
debug($push->mensagensView(1));
echo '<h1>Mensagens Devices All</h1>';
debug($push->mensagensDevices());

echo '<h1>Mensagens Devices Detalhes</h1>';
debug($push->mensagensDevicesView(1));
```