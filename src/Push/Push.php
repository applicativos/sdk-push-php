<?php

//
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Push
 *
 * @author lucas
 */

namespace Sdk\Push;

use Curl\Curl;

class Push {

    //put your code here

    private $config = [
        'token' => '',
        'url' => 'http://push.servicesw.com.br/api/'
    ];
    public $code = 200;

    public function __construct($config) {
        $this->config = array_merge($this->config, $config);
        $this->config['url'] = trim($this->config['url'], '/') . '/';
    }

    public function info() {
        return $this->exec('GET', 'apps/view.json');
    }

    public function configView($chave) {
        return $this->exec('GET', 'apps-configs/view/' . $chave . '.json');
    }

    public function configAdd($params = array()) {
        return $this->exec('POST', 'apps-configs/edit-personalizado.json', $params);
    }

    public function app($params = array()) {
        return $this->exec('POST', 'apps/add.json', $params);
    }

    public function devices($params = array()) {
        return $this->exec('GET', 'devices/index.json', $params);
    }

    public function devicesAdd($params) {
        return $this->exec('POST', 'devices/add.json', $params);
    }

    public function devicesEdit($id, $params) {
        return $this->exec('PUT', "devices/edit/{$id}.json", $params);
    }

    public function devicesDelete($id) {
        return $this->exec('DELETE', "devices/delete/{$id}.json");
    }

    public function devicesView($id) {
        return $this->exec('GET', "devices/view/{$id}.json");
    }

    public function mensagens($params = array()) {
        return $this->exec('GET', 'push-mensagens/index.json', $params);
    }

    public function mensagensAdd($params) {
        return $this->exec('POST', 'push-mensagens/add.json', $params);
    }

    public function mensagensEdit($id, $params) {
        return $this->exec('PUT', "push-mensagens/edit/{$id}.json", $params);
    }

    public function mensagensDelete($id) {
        return $this->exec('DELETE', "push-mensagens/delete/{$id}.json");
    }

    public function mensagensView($id) {
        return $this->exec('GET', "push-mensagens/view/{$id}.json");
    }

    public function mensagensDevices($params = array()) {
        return $this->exec('GET', 'push-mensagens-devices/index.json', $params);
    }

    public function mensagensDevicesView($id) {
        return $this->exec('GET', "push-mensagens-devices/view/{$id}.json");
    }

    private function exec($type, $url, $params = array()) {
        $curl = new Curl();
        $curl->setHeader('X-ApiToken', $this->config['token']);
        $curl->setHeader('Content-Type', 'application/json');
        switch (strtoupper($type)) {
            case 'GET':
                $curl->get($this->config['url'] . $url, $params);
                break;

            case 'POST':
                $curl->post($this->config['url'] . $url, json_encode($params));
                break;

            case 'PUT':
                $curl->put($this->config['url'] . $url, json_encode($params));
                break;

            case 'DELETE':
                $curl->delete($this->config['url'] . $url, $params);
                break;

            default:
                break;
        }
        $this->code = $curl->http_status_code;
        $resp = json_decode($curl->response, false);
        $curl->close();
        return $resp;
    }

}
